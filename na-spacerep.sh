#!/bin/bash
#
# This script prints a nice report for space usage on Netapp filers.
#
# Both 7mode (<=8.2) and CDOT (<=8.3) are supported
# SSH key recommended!

# defaults
path=".*"
user="root"
ssh_port=22
sort="-k6"
type=0
grepo="-e"


# help function
print_help() {
    cat <<EOF | expand -t 4

Report space usage for a given Netapp Filer:/path/

  Usage: ${0} -f <filer> [-x <path>] [-u <user>] [-p <port>] [-s <key>] [-t <type>] [-ircdh]

    -f <filer>  Netapp Filer to connect to.
    -x <path>   Filter by <path>. It can be a keyword or a single-quoted regex.
    -u <user>   Alternative username. The default is ${user}.
    -p <port>   Alternative SSH port.
    -s <key>    Sort by <key>. Available keys are:
                    L : label
                    U : used space
                    F : free space
                    T : total space
                    P : percentage
                    N : path name -default
	-t <type>	Select what to show:
					0 : volumes and qtrees -default
					1 : snapshots
					2 : volumes, qtrees and snapshots
					3 : aggregates and their snapshots
    -i          Ignore case in path pattern.
    -r          Reverse the output.
    -c          Apply some color to important things.
    -d          Print ssh raw output for debug: '-s' and '-r' will be ignored.
    -h          Print this help.

  Examples:
    ${0} -f filer01 -x volume1
    ${0} -u admin -f filer99 -x 'vol.*AMM[0-5]' -cs P
    ${0} -f filer_n -x 'vol\/data_[^a-z]' -d
EOF
}


# a bunch of commands sent to the filer
send_cmd() {
	if [[ "${type}" -lt 3 ]] ; then
		command='df -k ; quota report ; vfiler run "*" quota report'
	else
		command='df -A'
	fi
}

# remove/replace some strings from raw output
# to standardize snap path
fix_out() {
    sed '
    s/reserve//g ;
    s/\.\./\[snapshots\]/g ;
    s/\.snapshot/\[snapshots\]/g ;
	1d ;
	/^\s*$/d ;
	/.*entries were displayed.*/d ;
    '
}

# keystone function: do the math and format the final output
proc_values() {
    # Variable escape fix suggested by Jon Snow:
    local awk_filter="${path//\\/\\\\}"

    awk -v pattern="${awk_filter}" -v icase="${icase}" '
    BEGIN {
        # check ignorecase option
        if ( icase == 1 ) {
            IGNORECASE=1 ;
            caselab=" (ignore case)" ;
        }

        # print a nice header
        print "" ;
        printf "%12s","" ;
        printf "%12s","Used GB" ;
        printf "%12s","Free GB" ;
        printf "%12s","Total GB" ;
        printf "%13s","Perc.used" ;
        printf "\tFilter"caselab": " ;
        print "/"pattern"/" ;
    }

    $0 ~ pattern {
        # columns to use from raw output
        C_filesystem=$1 ;

        # if volume
        if ( C_filesystem ~ /\/vol\// || C_filesystem ~ /snap/ ) {
            C_total=$2 ;
            C_used=$3 ;
            C_mount=$6 ;

            # search for "KB", then calculate used, else set to zero
            if ((x=index(C_used,"KB")) > 0) {
                used=(substr(C_used,1,x-1))/1024/1024 ;
            } else { used=0    ; }

            # same as before but for total
            if ((x=index(C_total,"KB")) > 0) {
                total=(substr(C_total,1,x-1))/1024/1024 ;
            } else { total=0 ; }

        # if quota 7mode (always in KB)
        } else if ( C_filesystem == "tree" ) {
            C_total=$6 ;
            C_used=$5 ;
            C_mount=$9 ;

            # calc used and total GB
            if (C_used ~ /[0-9]/) {
                used=C_used/1024/1024 ;
            } else { used=0 ; }

            if (C_total ~ /[0-9]/) {
                total=C_total/1024/1024 ;
            } else { total=0 ; }

        # if quota CDOT
        } else if ($3 == "tree") {
            C_total=$6 ;
            C_used=$5 ;
            C_mount="/vol/"$1"/"$2 ;

            # convert to GB
            if (C_used ~ /[0-9]B/) {
                x=index(C_used,"B") ;
                used=(substr(C_used,1,x-1))/1024/1024/1024 ;
            } else if (C_used ~ /[0-9]KB/) {
                x=index(C_used,"KB") ;
                used=(substr(C_used,1,x-1))/1024/1024 ;
            } else if (C_used ~ /[0-9]MB/) {
                x=index(C_used,"MB") ;
                used=(substr(C_used,1,x-1))/1024 ;
            } else if (C_used ~ /[0-9]GB/) {
                x=index(C_used,"GB") ;
                used=(substr(C_used,1,x-1)) ;
            } else if (C_used ~ /[0-9]TB/) {
                x=index(C_used,"TB") ;
                used=(substr(C_used,1,x-1))*1024 ;
            } else { used=0 ; }

            if (C_total ~ /[0-9]B/) {
                x=index(C_total,"B") ;
                total=(substr(C_total,1,x-1))/1024/1024/1024 ;
            } else if (C_total ~ /[0-9]KB/) {
                x=index(C_total,"KB") ;
                total=(substr(C_total,1,x-1))/1024/1024 ;
            } else if (C_total ~ /[0-9]MB/) {
                x=index(C_total,"MB") ;
                total=(substr(C_total,1,x-1))/1024 ;
            } else if (C_total ~ /[0-9]GB/) {
                x=index(C_total,"GB") ;
                total=(substr(C_total,1,x-1)) ;
            } else if (C_total ~ /[0-9]TB/) {
                x=index(C_total,"TB") ;
                total=(substr(C_total,1,x-1))*1024 ;
            } else { total=0 ; }
			
		# if aggregates
        } else if ( C_filesystem ~ /^[Aa][Gg][Gg][Rr]/ && C_filesystem !~ /^Aggregate/ ) {
			C_total=$2 ;
            C_used=$3 ;
            C_mount=C_filesystem ;
			total=C_total/1024/1024
			used=C_used/1024/1024
		}

        # get free Gbyte
        # value from raw output not used:
        # we need negative values to know the overflow amount (quotas and snapshots)
        free=total-used ;

        # calculate percentage but cannot divide by zero
        if ( total == 0 ) {
            perc=-1 ;
        } else {
            perc=used/total*100 ;
        }

        # set a label based on percentage
        if ( perc > 100 ) {
            label="overflow" ;
        } else if ( perc >= 98 ) {
            label="full" ;
        } else {
            label="ok" ;
        }

        # print nicely spaced table-like
        # values
        printf "%12s",label ;
        printf "%12.3f",used ;
        printf "%12.3f",free ;
        printf "%12.3f",total ;
        printf "%12.1f",perc ;
        printf "%%" ;

        # and path
        # fix for different outputs qtree/7-mode/CDOT
        if (C_filesystem ~ /\//) {
            printf "\t"C_filesystem ;
        } else {
            printf "\t"C_mount ;
        }
        print "" ;
    }

    END {exit;}
    '
}

# sort values except first N header lines
sort_values() {
    local headrows=2
    for lines in $(seq $headrows) ; do
        read -r
        printf "%s\n" "$REPLY"
    done

    sort ${sort} ${reverse}
}

# the color function
# coloring by sed-replacing regex patterns with colored ones
paint() {
    if [[ ${1} = "true" ]] ; then
        local norm="$(printf '\033[0;37m')"
        local head="$(printf '\033[1;37m')"
        local red="$(printf '\033[1;31m')"
        local yell="$(printf '\033[1;33m')"
        local grey="$(printf '\033[1;30m')"
        sed -r "\
        s/^.*Used.*used.*$/${head}&${norm}/g ;\
        s/ 9[4-9]\..\%/${yell}&${norm}/g ;\
        s/ignore case/${yell}&${head}/g ;\
        s/ [0-9][0-9][0-9]+\..\%/${red}&${norm}/g ;\
        s/-([0-9]+)\.([0-9]+)[^%]/${red}&${norm}/g ;\
        s/\/vol\/.*\[snapshots\]/${grey}&${norm}/g ;\
        "
    else tee
    fi
}

# process and refine the raw output
refine() {
    if [[ "${ropt}" != "RAW" ]] ; then
        fix_out \
        | proc_values \
        | sort_values \
        | paint ${color}
    else
        grep "${grepo}" "${path}" #if raw, filter only
    fi
}

typer() {
	if [[ "${type}" = 0 ]] ; then
		grep -v '\[snapshots\]'
	elif [[ "${type}" = 1 ]] ; then
		grep '\[snapshots\]'
	else tee
	fi
}


# BEGIN
# get the options
while getopts ":p:s:f:x:u:t:irdch" OPT ; do
    case ${OPT} in
        p)  ssh_port="${OPTARG}" ;;
        s)
            if   [[ "${OPTARG}" = "L" ]] ; then sort="-k1"
            elif [[ "${OPTARG}" = "U" ]] ; then sort="-k2 -n"
            elif [[ "${OPTARG}" = "F" ]] ; then sort="-k3 -n"
            elif [[ "${OPTARG}" = "T" ]] ; then sort="-k4 -n"
            elif [[ "${OPTARG}" = "P" ]] ; then sort="-k5 -n"
            fi
            ;;
        i)  
			icase=1
			grepo="-i"
			;;
        r)  reverse="-r" ;;
        d)  ropt="RAW" ;;
        f)
            raw_filer="${OPTARG}"
            filer="${raw_filer//[^a-zA-Z0-9\-\.\_]/}" # anti-injection!
            ;;
        x)  path="${OPTARG}"    ;;
        c)  color="true" ;;
        u)  user="${OPTARG}" ;;
		t)	type="${OPTARG}" ;;
        h)
            print_help
            exit 0
            ;;
        \?)
            echo -e "\nInvalid option: -${OPTARG}" 1>&2
            print_help | grep -B1 ' Usage:'
            exit 1
            ;;
        :)
            echo -e "\nArgument required for option: -${OPTARG}" 1>&2
            print_help | grep -B1 ' Usage:'
            exit 1
            ;;
    esac
done

# arg check
if [[ ${#} -ge 2 ]] && [[ ${@} =~ '-f ' ]] ; then
    echo -n #booooring!
else
    print_help
    exit 1
fi

send_cmd

# connect and get the raw output | process it
ssh -p ${ssh_port} ${user}@${filer} "${command}" 2>/dev/null | refine | typer

