# na-spacerep
*Report detailed space usage for a given Netapp filer and path*

This is a quite dirty script that get the data via SSH.

Both 7mode (<=8.2) and CDOT (<=8.3) are supported.

Use of SSH key is recommended!

Please read LICENSE.txt

### Dependencies
- bash (>= 3)
- standard unix/linux utils (ssh, sed, gawk, grep)

### Usage
```
bash na-spacerep.sh -f <filer> [-x <path>] [-u <user>] [-p <port>] [-s <key>] [-t <type>] [-ircdh]

    -f <filer>  Netapp Filer to connect to.
    -x <path>   Filter by <path>. It can be a keyword or a single-quoted regex.
    -u <user>   Alternative username. The default is ${user}.
    -p <port>   Alternative SSH port.
    -s <key>    Sort by <key>. Available keys are:
            L : label
            U : used space
            F : free space
            T : total space
            P : percentage
            N : path name -default
    -t <type> 	Select what to show:
		    0 : volumes and qtrees -default
		    1 : snapshots
		    2 : volumes, qtrees and snapshots
		    3 : aggregates and their snapshots
    -i          Ignore case in path pattern.
    -r          Reverse the output.
    -c          Apply some color to important things.
    -d          Print ssh raw output for debug: '-s' and '-r' will be ignored.
    -h          Print this help.

  Examples:
    ${0} -f filer01 -x volume1
    ${0} -u admin -f filer99 -x 'vol.*AMM[0-5]' -cs P
    ${0} -f filer_n -x 'vol\/data_[^a-z]' -d
```
